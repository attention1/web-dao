import { LitElement, html } from "lit";

export default class AudioFile extends LitElement {
  static get properties() {
    return {
      src: { state: true },
      widget: { state: true },
    };
  }
  constructor() {
    super();
    this.src = "";
  }
  connectedCallback() {
    super.connectedCallback();
    if (this.widget) {
      if (this.widget.file) {
        this.src = URL.createObjectURL(this.widget.file);
      }
    }
  }
  render() {
    return html`
      ${this.src ? html`<audio controls src=${this.src}></audio>` : null}
      <input type="file" @change=${this.onInput}>select file</button>
      `;
  }

  onInput(event) {
    event.preventDefault();
    const files = event.target.files;
    if (!files || files.length === 0) {
      return;
    }
    const file = files[0];
    this.src = URL.createObjectURL(file);
    this.dispatchEvent(new CustomEvent("update", { detail: file }));
  }

  createRenderRoot() {
    return this;
  }
}

window.customElements.define("audio-file", AudioFile);
