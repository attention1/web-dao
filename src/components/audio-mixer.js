import { LitElement, html } from "lit";

export default class AudioMixer extends LitElement {
  static get properties() {
    return {
      /* props */
      widgets: { type: Array, state: true },

      /* state */
      channels: { type: Array, state: true },
      isPlaying: { type: Boolean, state: true },
    };
  }

  constructor() {
    super();
    this.isPlaying = false;
    /* one widget = one channel, same index */
    this.widgets = [];
    this.channels = [];
  }

  async firstUpdated(changedProperties) {
    this.audio = window.AudioContext
      ? new AudioContext()
      : window.webkitAudioContext
      ? new webkitAudioContext()
      : undefined;

    if (!this.audio) {
      throw new Error("Missing audio context");
    }

    this.merger = this.audio.createChannelMerger(2);
    this.splitter = this.audio.createChannelSplitter(2);
    this.merger.connect(this.audio.destination);

    if (this.widgets && this.widgets.length) {
      await this.convertProjectToMix();
    }
  }

  async updated(changedProperties) {
    if (changedProperties.has("widgets")) {
      await this.convertProjectToMix();
    }
  }

  async convertProjectToMix() {
    // Create a new merger with the number of channels equal to the number of widgets
    if (this.widgets && this.widgets.length) {
      this.merger = this.audio.createChannelMerger(this.widgets.length);
      this.merger.connect(this.audio.destination);

      this.channels = await Promise.all(
        this.widgets.map((widget, index) =>
          this.widgetToAudioChannel(widget, index)
        )
      );
    } else {
      this.channels = [];
    }
    this.dispatchEvent(new CustomEvent("channels", { detail: this.channels }));
  }

  async widgetToAudioChannel(widget, widgetIndex) {
    if (!widget.file) return;

    const buffer = await new Blob([widget.file]).arrayBuffer();
    const audioBuffer = await this.audio.decodeAudioData(buffer);
    let source = this.audio.createBufferSource();
    let gainNode = this.audio.createGain();

    source.buffer = audioBuffer;
    source.connect(gainNode);
    gainNode.connect(this.merger, 0, widgetIndex);

    return { source, gainNode };
  }

  render() {
    return html`
      <ul>
        ${this.channels.map(
          (channel, index) =>
            html`<li>${this.renderVolumeSlider(channel, index)}</li>`
        )}
      </ul>
      ${this.channels.length ? this.renderPlayBtn() : null}
    `;
  }

  renderVolumeSlider(channel, index) {
    return html`
      <fieldset>
        <label for="volume${index}">${index + 1}:</label>
        ${channel
          ? html`<input
              type="range"
              id="volume${index}"
              name="volume${index}"
              min="0"
              max="1"
              step="0.01"
              value="1"
              @input=${(e) => this.setVolume(e, channel)}
            />`
          : "No audio in channel/widget"}
      </fieldset>
    `;
  }

  setVolume(e, channel) {
    channel.gainNode.gain.value = e.target.value;
  }

  renderPlayBtn() {
    return html`
      <button @click=${this.togglePlay}>
        ${this.isPlaying ? "Stop" : "Play"} mix
      </button>
    `;
  }

  async togglePlay() {
    if (!this.channels.length) return;
    // Ensure the AudioContext is not suspended
    if (this.audio.state === "suspended") {
      await this.audio.resume();
    }

    if (!this.isPlaying) {
      await this.convertProjectToMix();
      this.play();
    } else {
      this.stop();
    }
  }

  play() {
    this.channels.forEach((node) => {
      if (node && node.source) {
        node.source.start(0);
        node.started = true;
      }
    });
    this.isPlaying = true;
  }

  stop() {
    this.channels.forEach((node) => {
      if (node && node.source && node.started) {
        node.source.stop();
        node.source.disconnect(node.gainNode);
        node.gainNode.disconnect(this.merger);
        node.started = false;
      }
    });
    this.isPlaying = false;
  }

  createRenderRoot() {
    return this;
  }
}

window.customElements.define("audio-mixer", AudioMixer);
