// import the LitElement base class and html helper function
import { LitElement, html } from "lit";
import { exportMixedChannels } from "../libs/audio-exporter.js";

import {
  fileOpen,
  directoryOpen,
  fileSave,
  supported,
} from "browser-fs-access";

export default class ProjectManager extends LitElement {
  static get properties() {
    return {
      /* props */
      widgets: { type: Array, state: true },
      channels: { type: Array, state: true },
      /* state */
      dirHandle: { type: Object, state: true },
    };
  }

  get showProjectPicker() {
    return "showDirectoryPicker" in window && supported;
  }

  get widgetDataFolder() {
    return `widget_data`;
  }

  constructor() {
    super();
    window.addEventListener("beforeunload", this.onBeforeUnload.bind(this));
  }

  onBeforeUnload(event) {
    return;
    if (!window.confirm("Really close?")) {
      event.preventDefault();
    }
  }

  render() {
    return html`
      ${this.showProjectPicker && !this.dirHandle
        ? this.renderSelectDirectory()
        : null}
      ${this.dirHandle ? this.renderProject() : null}
      ${this.renderProjectStats()} ${this.renderProjectExportAudio()}
    `;
  }
  renderProject() {
    return html`
      <code title=${this.dirHandle.kind}>${this.dirHandle.name}</code>
      ${this.renderProjectControls()}
    `;
  }
  renderProjectControls() {
    if (!this.widgets || !this.widgets.length) {
      return html`<button @click=${this.openProject}>Open project</button>`;
    } else {
      return html`<button @click=${this.save}>Save File</button>`;
    }
  }
  renderSelectDirectory() {
    return html`
      <button @click=${this.selectProjectDirectory}>Select directory</button>
    `;
  }
  renderProjectExportAudio() {
    if (this.channels && this.channels.length) {
      return html`
        <button @click=${this.exportProjectAudio}>Export audio</button>
        <code>${this.channels.length} channels</code>
      `;
    }
  }

  renderProjectStats() {
    if (this.widgets) {
      return html` ${this.widgets.length} widgets`;
    }
  }

  async selectProjectDirectory() {
    try {
      this.dirHandle = await window.showDirectoryPicker();
    } catch (err) {
      console.error("Failed to open directory", err);
    }
  }

  async openProject() {
    if (!this.showProjectPicker) {
      console.error("Your browser does not support the File System Access API");
      return;
    }

    try {
      const project = await this.readProjectDirectory();
      console.log("project", project);
      this.dispatchEvent(new CustomEvent("project", { detail: project }));
    } catch (err) {
      console.error("Failed to open directory", err);
    }
  }

  async readProjectDirectory() {
    const files = await directoryOpen({
      recursive: true,
      mode: "readwrite",
      /* startIn: this.dirHandle.name, */
    });

    const widgetFiles = files.filter(
      (file) => file.type === "application/json"
    );
    const widgetDataFiles = files.filter(
      (file) => file.type !== "application/json"
    );

    const widgets = await Promise.all(
      widgetFiles.map(async (widgetFile) => {
        const text = await widgetFile.text();
        return JSON.parse(text);
      })
    );

    const widgetsWithData = widgets.map((widget) => {
      widget.file = widgetDataFiles.find((file) =>
        file.name.startsWith(widget.id)
      );
      return widget;
    });

    const project = {
      widgets: widgetsWithData,
    };
    return project;
  }

  save() {
    if (!this.project) return;
    if (this.project.widgets) {
      this.project.widgets.forEach((widget) => {
        console.info("Saving widget", widget);
        const widgetDataType = typeof widget.file;
        if (widget.file && widgetDataType === "object") {
          const widgetDataExt = widget.file.type.split("/")[1] || ".data";
          const blob = new Blob([widget.file], { type: widget.file.type });
          this.saveFile(`${widget.id}.${widgetDataExt}`, blob);
        }
        this.saveFile(`${widget.id}.json`, JSON.stringify(widget));
      });
    }
  }

  async saveFile(fileName, fileContent) {
    if (!this.dirHandle) {
      console.error("No directory selected");
      return;
    }

    const fileHandle = await this.dirHandle.getFileHandle(fileName, {
      create: true,
    });
    const writableStream = await fileHandle.createWritable();

    console.log(fileContent);
    try {
      await writableStream.write(fileContent);
      await writableStream.close();
      console.info("Saved file", fileName);
    } catch (err) {
      console.error("Failed to save file", fileName, err);
    }
  }

  async exportProjectAudio() {
    console.log("export project", this.project);
    if (!this.channels) {
      return;
    }
    const mixedAudio = exportMixedChannels(this.channels);
  }
  createRenderRoot() {
    return this;
  }
}

// define the new element
customElements.define("project-manager", ProjectManager);
