import { LitElement, html } from "lit";

export default class AudioRecorder extends LitElement {
  static get properties() {
    return {
      recording: { type: Boolean },
      audioChunks: { type: Array },
      mimeType: { type: String },
      src: { state: true },
      widget: { state: true },
    };
  }

  constructor() {
    super();
    this.recording = false;
    this.audioChunks = [];
    /* ['m4a', 'mp3', 'webm', 'mp4', 'mpga', 'wav', 'mpeg'] */
    /* this.mimeType = 'audio/webm; codecs=opus'; */
    if (MediaRecorder.isTypeSupported("audi/mp3")) {
      this.mimeType = "audio/mp3";
    } else if (MediaRecorder.isTypeSupported("audio/webm")) {
      this.mimeType = "audio/webm";
    }
  }

  connectedCallback() {
    super.connectedCallback();
    if (this.widget) {
      if (this.widget.file) {
        this.src = URL.createObjectURL(this.widget.file);
      }
    }
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    if (this.recording) {
      this._stopRecording();
    }
  }

  render() {
    return html`
      ${this.src && !this.recording
        ? html`<audio controls src=${this.src}></audio>`
        : null}
      <button @click=${this._toggleRecording} ?active=${!!this.recording}>
        ${this.recording ? "Stop" : !this.src ? "Start" : "Replace"} recording
      </button>
    `;
  }

  _toggleRecording(event) {
    event.preventDefault();
    if (this.recording) {
      this._stopRecording();
    } else {
      this._startRecording();
    }
  }

  async _startRecording() {
    const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
    this.mediaRecorder = new MediaRecorder(stream, { mimeType: this.mimeType });
    this.mediaRecorder.addEventListener("dataavailable", (event) => {
      if (event.data.size > 0) {
        this.audioChunks.push(event.data);
      }
    });
    this.mediaRecorder.addEventListener("stop", () => {
      const audioBlob = new Blob(this.audioChunks, { type: this.mimeType });
      const file = new File([audioBlob], `recording-${Date.now()}.webm`, {
        type: this.mimeType,
      });
      this.dispatchEvent(new CustomEvent("update", { detail: file }));
      this.src = URL.createObjectURL(file);
    });
    // if started with a number arguments, break mime-type
    this.mediaRecorder.start();
    this.recording = true;
  }

  _stopRecording() {
    this.recording = false;
    this.mediaRecorder.stop();
    this.audioChunks = [];
  }
  createRenderRoot() {
    return this;
  }
}

window.customElements.define("audio-recorder", AudioRecorder);
