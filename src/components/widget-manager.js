import { LitElement } from "lit";
import { html, unsafeStatic, literal } from "lit/static-html.js";

const ALL_WIDGETS = {
  "audio-file": {},
  "audio-recorder": {},
};

export default class WidgetManager extends LitElement {
  static get properties() {
    return {
      widgets: { type: Array },
    };
  }

  get widgetNames() {
    return Object.entries(ALL_WIDGETS).map(([name, config]) => {
      return name;
    });
  }

  constructor() {
    super();
    this.widgets = [];
  }

  addWidget(name) {
    const newWidget = {
      name: name || this.widgetNames[0],
      id: crypto.randomUUID(),
      file: null,
    };
    this.widgets = [...this.widgets, newWidget];
    this.dispatchEvent(
      new CustomEvent("widgets-changed", { detail: this.widgets })
    );
  }

  removeWidget(widgetToRemove) {
    this.widgets = this.widgets.filter(
      (widget) => widget.id !== widgetToRemove.id
    );
    this.dispatchEvent(new CustomEvent("widgets", { detail: this.widgets }));
  }

  updateWidget(widgetToUpdate, newWidgetData) {
    const newWidgets = [...this.widgets];
    const indexToUpdate = newWidgets.indexOf(widgetToUpdate);
    newWidgets[indexToUpdate] = {
      ...newWidgets[indexToUpdate],
      file: newWidgetData,
    };
    this.widgets = newWidgets;
    this.dispatchEvent(new CustomEvent("widgets", { detail: this.widgets }));
  }

  onNewWidget(event) {
    this.addWidget(event.target.value);
    this.querySelector("select").querySelector("option").selected = true;
  }

  render() {
    return html`
      <select @input=${this.onNewWidget}>
        <optgroup disabled>
          <option selected>New widget</option>
        </optgroup>
        ${this.widgetNames.map(
          (name) => html`<option value="${name}">${name}</option>`
        )}
      </select>
      <form>
        <ul>
          ${this.widgets.map((widget) => {
            const tag = literal`${unsafeStatic(widget.name)}`;
            const $widgetDom = html`<${tag} .widget=${widget} @update=${(
              event
            ) => this.updateWidget(widget, event.detail)}></${tag}>`;

            return html`
              <li>
                <fieldset>
                  <button
                    type="button"
                    @click=${() => this.removeWidget(widget)}
                  >
                    -
                  </button>
                  <label>
                    <code>${widget.name}</code>
                  </label>
                  ${$widgetDom}
                </fieldset>
              </li>
            `;
          })}
        </ul>
      </form>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

window.customElements.define("widget-manager", WidgetManager);
