import { LitElement, html } from "lit";

export default class WebDao extends LitElement {
  static get properties() {
    return {
      /* state updated by children components */
      widgets: { type: Array, state: true },
      channels: { type: Array, state: true },
    };
  }

  constructor() {
    super();
    this.widgets = [];
    this.channels = [];
  }

  /* when loading a project from project-manager */
  onProject({ detail }) {
    const { widgets, channels } = detail;
    if (widgets) {
      this.widgets = widgets;
    }
    if (channels) {
      this.channels = channels;
    }
  }

  onChannels(event) {
    const { detail } = event;
    this.channels = detail;
    console.info("on channels", detail);
  }

  onWidgets(event) {
    const { detail } = event;
    this.widgets = detail;
    console.info("on widgets", detail);
  }

  render() {
    return html`
      <details open>
        <summary>Project</summary>
        <project-manager
          .widgets=${this.widgets}
          .channels=${this.channels}
          @project=${this.onProject}
        ></project-manager>
      </details>
      <details open>
        <summary>Widgets</summary>
        <widget-manager
          .widgets=${this.widgets}
          @widgets=${this.onWidgets}
        ></widget-manager>
      </details>
      <details open>
        <summary>Mixer</summary>
        <audio-mixer
          .widgets=${this.widgets}
          @channels=${this.onChannels}
        ></audio-mixer>
      </details>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

window.customElements.define("web-dao", WebDao);
