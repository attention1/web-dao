/* https://chat.openai.com/share/a91c9015-f6b5-461e-9e14-0c040b6f9e5f */

export async function exportMixedChannels(channels) {
  // Get the length of the longest channel
  const longestLength = Math.max(
    ...channels.map(({ source }) => source.buffer.length)
  );

  const offlineCtx = new OfflineAudioContext({
    numberOfChannels: 1,
    length: longestLength,
    sampleRate: 44100,
  });

  channels.forEach(({ source, effects }) => {
    let offlineSource = offlineCtx.createBufferSource();
    offlineSource.buffer = source.buffer;

    let lastNode = offlineSource;

    if (effects) {
      effects.forEach((effectNode) => {
        const offlineEffect = new offlineCtx[effectNode.constructor.name]();
        Object.assign(offlineEffect, effectNode);

        lastNode.connect(offlineEffect);
        lastNode = offlineEffect;
      });
    }

    lastNode.connect(offlineCtx.destination);

    offlineSource.start(0);
  });

  const renderedBuffer = await offlineCtx.startRendering();

  const combinedChannelData = new Float32Array(longestLength);
  for (let i = 0; i < renderedBuffer.numberOfChannels; i++) {
    const channelData = renderedBuffer.getChannelData(i);
    for (let j = 0; j < channelData.length; j++) {
      combinedChannelData[j] += channelData[j];
    }
  }

  const audioBlob = await bufferToBlob(combinedChannelData);

  return audioBlob;
}

function bufferToBlob(buffer) {
  return new Promise((resolve, reject) => {
    const worker = new Worker(
      URL.createObjectURL(
        new Blob([
          `
      self.addEventListener('message', e => {
        const data = new Float32Array(e.data.input);
        const result = new Int16Array(data.length);

        for (let i = 0; i < data.length; i++) {
          result[i] = Math.max(-32768, Math.min(32767, data[i] * 32767.5));
        }

        const blob = new Blob([result.buffer], { type: 'audio/wav' });

        self.postMessage(blob, [blob]);
      }, false);
          `,
        ])
      )
    );

    worker.addEventListener("message", (e) => resolve(e.data));
    worker.addEventListener("error", reject);

    // pass the buffer by copying
    worker.postMessage({ input: Array.from(buffer) });
  });
}
